package dk.kea.swd.exam3sem;

import java.time.LocalDate;

/**
 * Created by Laura Gadola on 27-Nov-15.
 */
public class Education {
    private String school;
    private String eduLevel;
    private String studyField;
    private LocalDate startDate;
    private LocalDate endDate;
    private String description;

    public Education(String school, String eduLevel, String studyField, LocalDate startDate, LocalDate endDate, String description) {
        this.school = school;
        this.eduLevel = eduLevel;
        this.studyField = studyField;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
    }

    public String getSchool() {
        return school;
    }

    public String getEduLevel() {
        return eduLevel;
    }

    public String getStudyField() {
        return studyField;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Education{" +
                "school='" + school + '\'' +
                ", eduLevel='" + eduLevel + '\'' +
                ", studyField='" + studyField + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                '}';
    }
}
