package dk.kea.swd.exam3sem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Cris on 27-Nov-15.
 */
public class Navigator {
    private static Stage stage;
    private static boolean isMenuPage = true;

    public enum Page {EmployerProfile, AddJobOffer, JobSeekerProfile, JobViever, LoginViewer, Menu, Application, ApplicantViewer,ViewApplication}

    private static Map<Page, String> resourceMap = new HashMap<>();

    public static void showNew(Page p) {
        stage = new Stage();
        show(p);
    }
    public static void show(Page p) {
        String resourceName = resourceMap.get(p);
        Parent root;
        try {
            root = FXMLLoader.load(Main.class.getResource(resourceName));
            stage.setTitle("JobOffer Finder");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(isMenuPage) {
            stage = new Stage();
            isMenuPage = false;
        }
    }

    public static void setNavigator(Stage stage) {
        Navigator.stage = stage;
        setMap();
    }

    private static void setMap() {
        resourceMap.put(Page.EmployerProfile,   "fxml/EmployerProfile.fxml");
        resourceMap.put(Page.AddJobOffer,       "fxml/AddJobOffer.fxml");
        resourceMap.put(Page.JobSeekerProfile,  "fxml/JobSeekerProfile.fxml");
        resourceMap.put(Page.JobViever,         "fxml/JobViewer.fxml");
        resourceMap.put(Page.LoginViewer,       "fxml/LoginViewer.fxml");
        resourceMap.put(Page.Application,       "fxml/Application.fxml");
        resourceMap.put(Page.Menu,              "fxml/Menu.fxml");
        resourceMap.put(Page.ApplicantViewer,   "fxml/ApplicantViewer.fxml");
        resourceMap.put(Page.ViewApplication,   "fxml/ApplicationViewer.fxml");//
    }
}
