package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.*;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ApplicantViewerController implements Initializable{
    @FXML TableView<JobSeeker> applicantsTable;
    @FXML TableColumn<JobSeeker, String> nameCol;
    @FXML TableColumn<JobSeeker, String> levelCol;
    @FXML TableColumn<JobSeeker, String> eduCol;
    @FXML TableColumn<JobSeeker, String> jobCol;
    @FXML TableColumn<JobSeeker, Integer> expYearsCol;
    @FXML TableColumn<JobSeeker, String> profileCol; // Button
    @FXML ListView<String> eduFilterList;
    @FXML Label yearsFilterLabel;
    @FXML Slider yearsFilterSlider;

//    @FXML
//    ListView<String> categoryListView;

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        applicantsTable.setItems(DAO.getJobSeekers());

        // Source: https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/TableColumn.html
//        nameCol.setCellValueFactory(new PropertyValueFactory<JobOffer, String>("name"));
        nameCol.setCellValueFactory(jobSeeker -> new ReadOnlyObjectWrapper(jobSeeker.getValue().getName()));
        eduCol.setCellValueFactory(jobSeeker -> new ReadOnlyObjectWrapper(jobSeeker.getValue().getEducation().getStudyField()));
        levelCol.setCellValueFactory(jobSeeker -> new ReadOnlyObjectWrapper(jobSeeker.getValue().getEducation().getEduLevel()));
        jobCol.setCellValueFactory(jobSeeker -> new ReadOnlyObjectWrapper(jobSeeker.getValue().getExperience().getPosition()));
        expYearsCol.setCellValueFactory(jobSeeker -> new ReadOnlyObjectWrapper(jobSeeker.getValue().getExpYears()));

        profileCol.setCellFactory(column -> new ButtonTableCell("profile-button") {
            @Override
            public void onClick() {
                Navigator.showNew(Navigator.Page.ViewApplication);
            }
        });

        eduFilterList.setItems(DAO.getEducationLevels());
        eduFilterList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        yearsFilterSlider.setMax(10.0);
        yearsFilterSlider.setOnMouseDragged(event -> {
            int years = (int) yearsFilterSlider.getValue();
            yearsFilterLabel.setText(years + " years");
        });

        //
    }

}

