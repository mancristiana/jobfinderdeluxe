package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.Navigator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by Christina on 23/11/2015.
 */
public class LoginController  implements Initializable {

    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;
    @FXML private PasswordField confirmField;
    @FXML private Button loginButton;
    @FXML private Button registerButton;
    @FXML private Label forgotPassLabel;    // Forgot your password ?
    @FXML private Label infoLabel;
    @FXML private Label linkLabel;

    private ToggleGroup radioGroup;
    @FXML private RadioButton jobSeekerRadioBtn;
    @FXML private RadioButton employerRadioBtn;

    private boolean isLoginPage = true;

    HashMap<String,String> logins = new HashMap<>();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logins.put("admin@jobfinder.dk", "password");
        logins.put("p", "p");

        radioGroup = new ToggleGroup();
        jobSeekerRadioBtn.setToggleGroup(radioGroup);
        employerRadioBtn.setToggleGroup(radioGroup);

        setVisibility("login");
//        removeFocus();
    }

    @FXML
    private void loginClicked() {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (!logins.containsKey(username)) {
            infoLabel.setText("Username doesn't exist!");
        } else {
            if (!logins.get(username).equals(password)) {
                infoLabel.setText("Wrong Password!");
                forgotPassLabel.setVisible(true);
                forgotPassLabel.setText("Forgot your password?");
            } else {
                infoLabel.setText("");
                Navigator.show(Navigator.Page.EmployerProfile);
            }
        }
    }

    @FXML
    private void registerClicked() {
        String username = usernameField.getText();
        String password = passwordField.getText();
        String confirmPassword = confirmField.getText();

        if (logins.containsKey(username)) {
            infoLabel.setText("Username already taken!");
            usernameField.setText("");
        } else {
            if (password.length() > 0 && confirmPassword.length() > 0) {
                if (!password.equals(confirmPassword)) {
                    infoLabel.setText("Passwords do not match!");
                    passwordField.setText("");
                    confirmField.setText("");
                } else {
                    logins.put(username, password);
                    Navigator.show(Navigator.Page.EmployerProfile);
                }
            } else {
                infoLabel.setText("Password fields must be filled in!");
            }
        }
    }

    @FXML
    private void forgotClicked() {
        forgotPassLabel.setText("Instructions have been sent to your email!");
        infoLabel.setText("");
    }

    @FXML
    private void linkClicked() {
        isLoginPage = !isLoginPage; // Toggle between Login and Register pages
        if (isLoginPage) {
            setVisibility("login");
            linkLabel.setText("Register new Account");
        } else {
            setVisibility("register");
            linkLabel.setText("Go to Login");
        }

        // removeFocus();
        // registerButton.setFocusTraversable(true);
    }

//    private void removeFocus() {
//        usernameField.setFocusTraversable(false);
//        passwordField.setFocusTraversable(false);
//        confirmField.setFocusTraversable(false);
//    }

    private void clearText() {
        usernameField.setText("");
        passwordField.setText("");
        confirmField.setText("");
        infoLabel.setText("");
    }

    private void setVisibility(String visibility) {
        clearText();
        switch (visibility) {
            case "login" :
                // Fields
                passwordField.setVisible(true);
                confirmField.setVisible(false);

                // Buttons
                loginButton.setVisible(true);
                registerButton.setVisible(false);
                jobSeekerRadioBtn.setVisible(false);
                employerRadioBtn.setVisible(false);

                // Label
                forgotPassLabel.setVisible(false);
                break;
            case "register":
                // Fields
                passwordField.setVisible(true);
                confirmField.setVisible(true);

                // Buttons
                loginButton.setVisible(false);
                registerButton.setVisible(true);
                jobSeekerRadioBtn.setVisible(true);
                employerRadioBtn.setVisible(true);

                // Label
                forgotPassLabel.setVisible(false);
                break;
            default : break;
        }
    }

}