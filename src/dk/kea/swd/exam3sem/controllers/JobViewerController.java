package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.DAO;
import dk.kea.swd.exam3sem.JobOffer;
import dk.kea.swd.exam3sem.Navigator;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class JobViewerController implements Initializable {
    @FXML private TableView<JobOffer> tableView;
    @FXML private TableColumn<JobOffer, String> title;
    @FXML private TableColumn<JobOffer, String> category;
    @FXML private TableColumn<JobOffer, String> location;
    @FXML private TableColumn<JobOffer, String> employer;
    @FXML private TableColumn<JobOffer, LocalDate> dateAdded;
    @FXML private TableColumn<JobOffer, LocalDate> expiration;
    @FXML private Button apply;
    @FXML private ListView<String> categoryListView;
    @FXML private ListView<String> areaListView;
    @FXML private Pane jobDetailsPane;
    @FXML private Label positionLabel;
    @FXML private Label companyLabel;
    @FXML private Label locationLabel;
    @FXML private ImageView logoImageView;
    @FXML private Label endDateLabel;
    @FXML private TextArea descriptionArea;

    @Override
    public void initialize(URL url, ResourceBundle resources) {
        //Show categories
        ObservableList<String> categories = DAO.getJobCategories();
        categoryListView.setItems(categories);
        categoryListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Show areas
        ObservableList<String> areas = DAO.getAreas();
        areaListView.setItems(areas);
        areaListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Set Table items and columns
        ObservableList<JobOffer> list = DAO.getJobOffers();
        tableView.setItems(list);

        title.setCellValueFactory(new PropertyValueFactory<JobOffer, String>("title"));
        category.setCellValueFactory(new PropertyValueFactory<JobOffer, String>("category"));
        location.setCellValueFactory(new PropertyValueFactory<JobOffer, String>("location"));
        employer.setCellValueFactory(new PropertyValueFactory<JobOffer, String>("employer"));
        dateAdded.setCellValueFactory(new PropertyValueFactory<JobOffer, LocalDate>("dateAdded"));
        expiration.setCellValueFactory(new PropertyValueFactory<JobOffer, LocalDate>("expiration"));

        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    tableView.setMaxHeight(190);
                    jobDetailsPane.setVisible(true);
                    //fill in right info about the job selected
                    JobOffer job = tableView.getSelectionModel().getSelectedItem();
                    positionLabel.setText(job.getTitle());
                    companyLabel.setText(job.getEmployer());
                    locationLabel.setText(job.getLocation());
                    endDateLabel.setText(job.getExpiration().toString());
                    descriptionArea.setText(job.getDescription());
                    //Set logo
                    File file = new File("src/dk/kea/swd/exam3sem/pictures/" + job.getLogo());
                    Image image = new Image(file.toURI().toString());
                    logoImageView.setImage(image);
                }
            }
        });

        logoImageView.setOnMouseClicked(event -> {
            JobOffer job = tableView.getSelectionModel().getSelectedItem();
            DAO.setJobMediator(job);
            Navigator.show(Navigator.Page.EmployerProfile);
            System.out.println("clicked:" + job.getLogo() + " " + job.getDescription());

        });
    }

    @FXML
    private void applyClicked() {
        Navigator.show(Navigator.Page.Application);
    }
}
