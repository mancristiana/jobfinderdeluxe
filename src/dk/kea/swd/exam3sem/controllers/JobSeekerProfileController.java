package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.DAO;
import dk.kea.swd.exam3sem.Education;
import dk.kea.swd.exam3sem.WorkExperience;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Laura Gadola on 23-Nov-15.
 */
public class JobSeekerProfileController implements Initializable {
    @FXML private Button uploadBtn;
    @FXML private ListView<String> filesListView;
    @FXML private Label pictureLabel;
    @FXML private Button pictureBtn;
    @FXML private ImageView pictureImageView;
    @FXML private ComboBox<String> eduLevelBox;
    @FXML private TextField schoolField;
    @FXML private TextField studyTField;
    @FXML private TextArea eduDescriptionField;
    @FXML private DatePicker eduStartDate;
    @FXML private DatePicker eduEndDate;
    @FXML private TableView<Education> eduTable;
    @FXML private TableColumn eduLevelCol;
    @FXML private TableColumn studyFieldCol;
    @FXML private TableColumn schoolCol;
    @FXML private TableView<WorkExperience> workExperienceTable;
    @FXML private TableColumn positionCol;
    @FXML private TableColumn companyCol;

    ObservableList<String> uploadedFiles;
    ObservableList<Education> educationList;
    ObservableList<WorkExperience> workExperienceList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        eduLevelBox.setItems(DAO.getEducationLevels());
        uploadedFiles = DAO.getUploadedFiles();
        filesListView.setItems(uploadedFiles);

        //no visible list if empty
        if (filesListView.getItems().isEmpty())
            filesListView.setVisible(false);

        // Set Education Table items and columns
        educationList = DAO.getEducationList();
        eduLevelCol.setCellValueFactory(new PropertyValueFactory<Education, String>("eduLevel"));
        studyFieldCol.setCellValueFactory(new PropertyValueFactory<Education, String>("studyField"));
        schoolCol.setCellValueFactory(new PropertyValueFactory<Education, String>("school"));
        eduTable.setItems(educationList);
        eduTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Education selected = eduTable.getSelectionModel().getSelectedItem();

                schoolField.setText(selected.getSchool());
                eduLevelBox.getSelectionModel().select(selected.getEduLevel());
                studyTField.setText(selected.getStudyField());
                eduStartDate.setValue(selected.getStartDate());
                eduEndDate.setValue(selected.getEndDate());
                eduDescriptionField.setText(selected.getDescription());
            }
        });

        // Set Job Table items and columns
        workExperienceList = DAO.getWorkExperienceList();
        positionCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("position"));
        companyCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("company"));
        workExperienceTable.setItems(workExperienceList);
    }

    @FXML
    private void uploadBtnOnAction() {
        //Open file explorer
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        //Get selected file
        File file = fileChooser.showOpenDialog(new Stage());
        uploadedFiles.add(file.getName());

        if (!filesListView.isVisible())
            filesListView.setVisible(true);
        if (filesListView.getItems().size() > 1 && filesListView.getHeight() != 54)
            filesListView.setPrefHeight(54);
    }

    @FXML
    private void pictureBtnMouseEntered() {
        pictureLabel.setVisible(true);
        pictureBtn.setOpacity(0.2);
    }

    @FXML
    private void pictureBtnMouseExited() {
        pictureLabel.setVisible(false);
        pictureBtn.setOpacity(1);
    }

    @FXML
    private void pictureBtnOnAction() {
        //Open file explorer
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        //Get selected file
        File file = fileChooser.showOpenDialog(new Stage());
        //Set the image
        Image image = new Image(file.toURI().toString());
        pictureImageView.setImage(image);
    }

    @FXML
    private void addEduBtnOnAction() {
        String school = schoolField.getText();
        String eduLevel = eduLevelBox.getSelectionModel().getSelectedItem();
        String studyField = studyTField.getText();
        LocalDate startDate = eduStartDate.getValue();
        LocalDate endDate = eduEndDate.getValue();
        String description = eduDescriptionField.getText();

        Education education = new Education(school,eduLevel,studyField,startDate,endDate,description);
        educationList.add(education);
        eduTable.refresh();
    }
}