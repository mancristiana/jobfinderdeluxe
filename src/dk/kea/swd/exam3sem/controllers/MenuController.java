package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.Navigator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by Cris on 27/11/2015.
 */
public class MenuController {

    @FXML
    private void onEmployerProfileClicked() {
        Navigator.show(Navigator.Page.EmployerProfile);
    }

    @FXML
    private void onAddJobOfferClicked() {
        Navigator.show(Navigator.Page.AddJobOffer);
    }

    @FXML
    private void onJobSeekerProfileClicked() {
        Navigator.show(Navigator.Page.JobSeekerProfile);
    }

    @FXML
    private void onJobViewerClicked() {
        Navigator.show(Navigator.Page.JobViever);
    }

    @FXML
    private void onLoginViewerClicked() {
        Navigator.show(Navigator.Page.LoginViewer);
    }

    @FXML
    private void onApplicantViewerClicked() {
        Navigator.show(Navigator.Page.ApplicantViewer);
    }
    @FXML
    private void onApplicationClicked() {
        Navigator.show(Navigator.Page.Application);
    }

    @FXML private void onViewApplicationClicked() {
        Navigator.show(Navigator.Page.ViewApplication);
    }

}

