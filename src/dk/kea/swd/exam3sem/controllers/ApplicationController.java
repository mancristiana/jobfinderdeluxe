package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.CheckBoxTableCell;
import dk.kea.swd.exam3sem.DAO;
import dk.kea.swd.exam3sem.Education;
import dk.kea.swd.exam3sem.WorkExperience;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * Created by Laura Gadola on 30-Nov-15.
 */
public class ApplicationController implements Initializable {
    @FXML
    private TableColumn positionCol;
    @FXML
    private TableColumn companyCol;
    @FXML
    private TableColumn workChoice;
    @FXML
    private TableColumn eduLevelCol;
    @FXML
    private TableColumn studyFieldCol;
    @FXML
    private TableColumn schoolCol;
    @FXML
    private TableColumn eduChoice;
    @FXML
    private TableView<WorkExperience> workExperienceTable;
    @FXML
    private TableView<Education> educationTable;
    @FXML
    private ComboBox applicationComboBox;
    @FXML
    private TextArea whyYouTextArea;
    @FXML
    private TextArea commentsTextArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<WorkExperience> workExperienceList = DAO.getWorkExperienceList();
        ObservableList<Education> educationList = DAO.getEducationList();
        applicationComboBox.setItems(DAO.getExistingApplications());
        applicationComboBox.setValue("new");
        //Set columns and items for the work experience table
        positionCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("position"));
        companyCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("company"));
        workChoice.setCellValueFactory(new PropertyValueFactory<WorkExperience, Boolean>("selected"));
        workChoice.setCellFactory(column -> new CheckBoxTableCell() {});
        workExperienceTable.setItems(workExperienceList);

        //Set columns and items for the education table
        eduLevelCol.setCellValueFactory(new PropertyValueFactory<Education, String>("eduLevel"));
        studyFieldCol.setCellValueFactory(new PropertyValueFactory<Education, String>("studyField"));
        schoolCol.setCellValueFactory(new PropertyValueFactory<Education, String>("school"));
        eduChoice.setCellValueFactory(new PropertyValueFactory<Education, Boolean>("selected"));
        eduChoice.setCellFactory(column -> new CheckBoxTableCell() {});
        educationTable.setItems(educationList);
    }

    public void comboTesting(){

       String selectedApplication = applicationComboBox.getValue().toString();
       String applicationContent = DAO.getApplicationContent(selectedApplication);

       whyYouTextArea.setText(applicationContent);
       commentsTextArea.setText(applicationContent);
    }


}
