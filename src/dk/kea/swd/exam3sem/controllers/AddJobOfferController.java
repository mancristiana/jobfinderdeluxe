package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.DAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class AddJobOfferController implements Initializable {

    @FXML TextField titleTextField;
    @FXML ComboBox categoryComboBox;
    @FXML TextField addressTextField;
    @FXML TextField postalCodeTextField;
    @FXML TextField cityTextField;
    @FXML DatePicker datePicker;
    @FXML TextArea jobDescriptionTextArea;
    @FXML Button createJobOfferButton;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        titleTextField.setFocusTraversable(false);

        categoryComboBox.setItems(DAO.getJobCategories());
    }

    public void createJobOffer(){

        System.out.println("Job offer created");
    }

}
