package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.DAO;
import dk.kea.swd.exam3sem.Employer;
import dk.kea.swd.exam3sem.JobOffer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Laura Gadola on 26-Nov-15.
 */
public class EmployerProfileController implements Initializable{
    @FXML private ComboBox companyType;
    @FXML private Button logoBtn;
    @FXML private Label logoLabel;
    @FXML private ImageView logoImageView;
    @FXML private Button saveBtn;

    @FXML private TextField nameBox;
    @FXML private TextField cvrBox;
    @FXML private TextField addressBox;
    @FXML private TextField postCodeBox;
    @FXML private TextField cityBox;
    @FXML private TextField phoneBox;
    @FXML private TextField emailBox;

   @Override
    public void initialize(URL location, ResourceBundle resources) {
        JobOffer job = DAO.getJobMediator();
        companyType.setItems(DAO.getCompanyTypes());

        if(job != null) { // this is if is opened from JobViewer
            Employer employer = DAO.getEmployerByLogo(job.getLogo());

            companyType.getSelectionModel().select(employer.getType());

            nameBox.setText(employer.getName());
            cvrBox.setText(employer.getCVR());
            addressBox.setText(employer.getAddress());
            postCodeBox.setText(employer.getPostalCode()+"");
            cityBox.setText(employer.getCity());
            phoneBox.setText(employer.getPhoneNumber());
            emailBox.setText(employer.getEmail());

            File file = new File("src/dk/kea/swd/exam3sem/pictures/" + job.getLogo());
            Image image = new Image(file.toURI().toString());
            logoImageView.setImage(image);

            saveBtn.setVisible(false);
        }

    }

    @FXML
    private void logoBtnMouseEntered() {
        logoLabel.setVisible(true);
        logoBtn.setOpacity(0.2);
    }

    @FXML
    private void logoBtnMouseExited() {
        logoLabel.setVisible(false);
        logoBtn.setOpacity(1);
    }

    @FXML
    private void logoBtnOnAction() {
        //Open file explorer
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        //Get selected file
        File file = fileChooser.showOpenDialog(new Stage());
        //Set the image
        Image image = new Image(file.toURI().toString());
        logoImageView.setImage(image);
    }
}
