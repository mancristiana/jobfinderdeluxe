package dk.kea.swd.exam3sem.controllers;

import dk.kea.swd.exam3sem.CheckBoxTableCell;
import dk.kea.swd.exam3sem.DAO;
import dk.kea.swd.exam3sem.Education;
import dk.kea.swd.exam3sem.WorkExperience;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Nikolai on 03.12.2015.
 */
public class ApplicationViewerController implements Initializable {

    @FXML private TableView<WorkExperience> workTable;
    @FXML private TableColumn positionCol;
    @FXML private TableColumn companyCol;

    @FXML private TableView<Education> educationTable;
    @FXML private TableColumn eduLevelCol;
    @FXML private TableColumn studyFieldCol;
    @FXML private TableColumn schoolCol;

    @FXML private TableView<WorkExperience> workTableFull;
    @FXML private TableColumn positionCol2;
    @FXML private TableColumn companyCol2;

    @FXML private TableView<Education> educationTableFull;
    @FXML private TableColumn eduLevelCol2;
    @FXML private TableColumn studyFieldCol2;
    @FXML private TableColumn schoolCol2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        populateApplicationTables();
        populateProfileTables();

    }

    private void populateProfileTables() {
        // GET INFO ABOUT WORK AND EDUCATION
        ObservableList<WorkExperience> workExperienceList = DAO.getWorkExperienceList();
        ObservableList<Education> educationList           = DAO.getEducationList();

        // POPULATE WORK TABLE
        positionCol2.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("position"));
        companyCol2.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("company"));

        workTableFull.setItems(workExperienceList);

        // POPULATE EDUCATION TABLE
        eduLevelCol2.setCellValueFactory(new PropertyValueFactory<Education, String>("eduLevel"));
        studyFieldCol2.setCellValueFactory(new PropertyValueFactory<Education, String>("studyField"));
        schoolCol2.setCellValueFactory(new PropertyValueFactory<Education, String>("school"));
        educationTableFull.setItems(educationList);
    }

    private void populateApplicationTables() {
        // GET INFO ABOUT WORK AND EDUCATION
        ObservableList<WorkExperience> workExperienceList = DAO.getWorkExperienceList();
        ObservableList<Education> educationList           = DAO.getEducationList();

        educationList.remove(0,2);
        workExperienceList.remove(0,3);

        // POPULATE WORK TABLE
        positionCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("position"));
        companyCol.setCellValueFactory(new PropertyValueFactory<WorkExperience, String>("company"));
        workTable.setItems(workExperienceList);

        // POPULATE EDUCATION TABLE
        eduLevelCol.setCellValueFactory(new PropertyValueFactory<Education, String>("eduLevel"));
        studyFieldCol.setCellValueFactory(new PropertyValueFactory<Education, String>("studyField"));
        schoolCol.setCellValueFactory(new PropertyValueFactory<Education, String>("school"));
        educationTable.setItems(educationList);
    }


}
