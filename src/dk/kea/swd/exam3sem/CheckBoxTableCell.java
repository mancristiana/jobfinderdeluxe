package dk.kea.swd.exam3sem;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.input.MouseEvent;

import java.awt.event.ActionEvent;

/**
 * Created by Laura Gadola on 30-Nov-15.
 */
public class CheckBoxTableCell extends TableCell<Object,String> {
    private CheckBox checkBox;

    public CheckBoxTableCell() {
        checkBox = new CheckBox();
        setAlignment(Pos.CENTER);
        checkBox.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                onClick();
            }
        });
    }

    @Override
    protected void updateItem(String t, boolean empty) {
        super.updateItem(t, empty);
        if(!empty) {
            setGraphic(checkBox);
            checkBox.setSelected(true);
        }
    }

    public void onClick() {
        if (checkBox.isSelected())
            getTableRow().setOpacity(1);
        else {
            getTableRow().setOpacity(0.3);
        }
    };
}
