package dk.kea.swd.exam3sem;

import java.time.LocalDate;

/**
 * Created by Laura Gadola on 16-Nov-15.
 */
public class JobOffer {
    String title;
    String category;
    String employer;
    String location;
    LocalDate dateAdded;
    LocalDate expiration;
    String description;
    String logo;

    //Deadline, description and logo are optional
    public JobOffer(String title, String category, String employer, String location) {
        this(title, category, employer, location, null, null, "", "");
    }

    public JobOffer(String title, String category, String employer, String location, LocalDate dateAdded, LocalDate expiration, String description, String logo) {
        this.title = title;
        this.category = category;
        this.employer = employer;
        this.location = location;
        setDateAdded(dateAdded);
        setExpiration(expiration);
        this.description = description;
        this.logo = logo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDate getExpiration() {
        return expiration;
    }

    public void setExpiration(LocalDate expiration) {
        if(expiration == null) {
            this.expiration = getDateAdded().plusMonths(3);
        } else
            this.expiration = expiration;
    }

    public LocalDate getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDate dateAdded) {
        if(dateAdded == null) {
            this.dateAdded = LocalDate.now();
        } else
            this.dateAdded = dateAdded;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "JobOffer{" +
                "title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", employer='" + employer + '\'' +
                ", location='" + location + '\'' +
                ", dateAdded=" + dateAdded +
                ", expiration=" + expiration +
                ", description='" + description + '\'' +
                '}';
    }
}
