package dk.kea.swd.exam3sem;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

/**
 * Created by Cristiana Man on 3-Dec-15.
 */
public abstract class ButtonTableCell extends TableCell<JobSeeker,String> {
    private Button button;

    public ButtonTableCell(String id) {
        button = new Button();
        setAlignment(Pos.CENTER);
        button.setId(id);
        button.setOnMouseClicked(event -> onClick());
    }

    @Override
    protected void updateItem(String t, boolean empty) {
        super.updateItem(t, empty);
        if(!empty) {
            setGraphic(button);
        }
    }

    public abstract void onClick();
}
