package dk.kea.swd.exam3sem;

import java.time.LocalDate;

/**
 * Created by Laura Gadola on 27-Nov-15.
 */
public class WorkExperience {

    private String company;
    private String position;
    private LocalDate startDate;
    private LocalDate endDate;
    private String description;

    public WorkExperience(String company, String position, LocalDate startDate, LocalDate endDate, String description) {
        this.company = company;
        this.position = position;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public String getPosition() {
        return position;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Job{" +
                "company='" + company + '\'' +
                ", position='" + position + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                '}';
    }
}
