package dk.kea.swd.exam3sem;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikolai on 19.11.2015.
 */
public class DAO {

    private static JobOffer jobMediator;

    public static ObservableList<String> getJobCategories() {
        ObservableList<String> jobCategories = FXCollections.observableArrayList();
        jobCategories.add("Information Technologies");
        jobCategories.add("Engineering");
        jobCategories.add("Management");
        jobCategories.add("Trade and Service");
        jobCategories.add("Sales");
        jobCategories.add("Education");
        jobCategories.add("Office & Economy");
        jobCategories.add("Health Care");
        jobCategories.add("Other");

        return jobCategories;
    }

    public static ObservableList<String> getAreas() {
        ObservableList<String> areas = FXCollections.observableArrayList();
        areas.add("Lyngby");
        areas.add("Roskilde");
        areas.add("Copenhagen");
        areas.add("Odense");
        areas.add("Aarhus");
        areas.add("Helsingor");

        return areas;
    }

    public static ObservableList<String> getCompanyTypes() {
        ObservableList<String> companyTypes = FXCollections.observableArrayList();
        companyTypes.add("I/S");
        companyTypes.add("IVS");
        companyTypes.add("ApS");
        companyTypes.add("A/S");
        companyTypes.add("K/S");
        companyTypes.add("P/S");
        companyTypes.add("A.M.B.A.");
        companyTypes.add("F.M.B.A.");
        companyTypes.add("S.M.B.A");
        companyTypes.add("G/S");
        companyTypes.add("Other");

        return companyTypes;
    }

    public static ObservableList<JobOffer> getJobOffers() {
        ObservableList<JobOffer> list = FXCollections.observableArrayList();
        LocalDate date = LocalDate.of(2015, 11, 19);
        list.add(new JobOffer("Software Engineer", "Information Technologies", "CobHam", "Lyngby", date, null, "DESCRIPTION", "Cobham.jpg"));
        list.add(new JobOffer("Mechanical Des. Engineer", "Engineering", "Sonion", "Roskilde", date.minusDays(12), null, "", "sonion.jpg"));
        list.add(new JobOffer("Head Manager", "Management", "Coca-Cola", "Copenhagen", date.minusDays(2), null, DAO.getApplicationContent("Soda soda!! SODAA"), "coca-cola.png"));
        list.add(new JobOffer("Waiter", "Trade and Service", "McDonalds", "Copenhagen", date.minusDays(7), null, "", "McDonalds.png"));
        list.add(new JobOffer("Sale Consultant", "Sales", "Imerco", "Odense", date.minusDays(1), null, "", "imerco.jpg"));
        list.add(new JobOffer("SWD Teacher", "Education", "Aarhus University", "Aarhus", date.minusDays(12), null, "", "Aarhus_University.png"));
        list.add(new JobOffer("Accountant", "Office & Economy", "FinPro", "Helsingor", date.minusDays(7), null, "", "FinPro.jpg"));
        list.add(new JobOffer("Massage Therapist", "Health Care", "MassPro", "Roskilde", date.minusDays(22), null, "", "massage.png"));
        list.add(new JobOffer("Cardiologist", "Health Care", "Rigshospitalet", "Copenhagen", date.minusDays(2), null, "", "Rigshospital.png"));

        // JobOffer(String title, String category, String company, String location, String date, String deadline, String description)

        return list;
    }

    public static ObservableList<String> getEducationLevels() {
        ObservableList<String> educationLevels = FXCollections.observableArrayList();
        educationLevels.add("AP");
        educationLevels.add("BA");
        educationLevels.add("MA");
        educationLevels.add("PhD");
        educationLevels.add("Other");

        return educationLevels;
    }

    public static ObservableList<Education> getEducationList() {
        ObservableList<Education> educationList = FXCollections.observableArrayList();
        educationList.add(new Education("KEA", "AP", "Computer Science",    LocalDate.of(2014, 8, 25), LocalDate.of(2017, 1, 30), "This is some bla bla bla"));
        educationList.add(new Education("KEA", "BA", "Architecture",        LocalDate.of(2015, 9, 1), LocalDate.of(2018, 7, 1), "This is description text"));
        educationList.add(new Education("KEA", "AP", "Multimedia Design",   LocalDate.of(2014, 3, 1), LocalDate.of(2017, 5, 30), "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo"));
        educationList.add(new Education("ITU", "PhD", "Some fancy name",    LocalDate.of(2015, 8, 25), LocalDate.of(2017, 1, 30), "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"));
        return educationList;
    }

    public static ObservableList<String> getUploadedFiles() {
        ObservableList<String> files = FXCollections.observableArrayList();
        files.add("profile_pic.png");
        files.add("CV_2015.pdf");
        files.add("234fxxc.txt");
        return files;
    }

    public static ObservableList<WorkExperience> getWorkExperienceList() {
        ObservableList<WorkExperience> workExperienceList = FXCollections.observableArrayList();
        workExperienceList.add(new WorkExperience("Cobaltix", "Intern", LocalDate.of(2016,1,25), LocalDate.of(2017, 1, 22), "One year internship"));
        workExperienceList.add(new WorkExperience("K Systemer", "Part time programmer", LocalDate.of(2015,4,25), LocalDate.of(2017, 1, 22), "Bla bla"));
        workExperienceList.add(new WorkExperience("Google", "Intern", LocalDate.of(2016,1,25), LocalDate.of(2017, 1, 22), "One year internship"));
        workExperienceList.add(new WorkExperience("Oracle", "Intern", LocalDate.of(2016,1,25), LocalDate.of(2017, 1, 22), "One year internship"));
        workExperienceList.add(new WorkExperience("Microsoft", "Intern", LocalDate.of(2016,1,25), LocalDate.of(2017, 1, 22), "One year internship"));

        return workExperienceList;
    }

    public static ObservableList<JobSeeker> getJobSeekers() {
        ObservableList<JobSeeker> jobSeekerList = FXCollections.observableArrayList();
        ObservableList<Education> eduList = getEducationList();
        ObservableList<WorkExperience> workList = getWorkExperienceList();

        jobSeekerList.add(new JobSeeker(1, "Nikolai Kalistratov", eduList.get(0), workList.get(0), 3));
        jobSeekerList.add(new JobSeeker(2, "Laura Gadola",        eduList.get(1), workList.get(1), 6));
        jobSeekerList.add(new JobSeeker(3, "Christina", eduList.get(2), workList.get(2), 9));
        jobSeekerList.add(new JobSeeker(4, "Cristiana", eduList.get(3), workList.get(3), 1));

        return jobSeekerList;
    }

    public static ObservableList<String> getExistingApplications() {
        ObservableList<String> applications = FXCollections.observableArrayList();
        applications.add("new");
        applications.add("Java Developer");
        applications.add("PHP Developer");
        applications.add("Database Administrator");

        return applications;
    }

    public static String getApplicationContent(String ApplicationTitle){

        String getApplicationContent = "IPSUM is simply dummy text of the printing and typesetting industry.\n" +
                "Lorem IPSUM has been the industry's standard dummy text ever since the 1500s, when an unknown\n" +
                "printer took a galley of type and scrambled it to make a type specimen book. It has survived IPSUM\n" +
                "only five centuries, but also the leap into IPSUM typesetting, remaining essentially unchanged.\n" +
                "It was popularised in the 1960s with the release of Letraset sheets containing Lorem IPSUM passages,\n" +
                "and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem IPSUM .";

        switch(ApplicationTitle){

            case "new":
                getApplicationContent = "";
            break;

            case "Java Developer":
                getApplicationContent = getApplicationContent.replace("IPSUM","JAVA Developer");
            break;

            case "PHP Developer":
                getApplicationContent = getApplicationContent.replace("IPSUM","PHP Developer");
            break;

            case "Database Administrator":
                getApplicationContent = getApplicationContent.replace("IPSUM","MYSQL Developer");
            break;
            default:    getApplicationContent = getApplicationContent.replace("IPSUM",ApplicationTitle);break;
        }

        return getApplicationContent;
    }


    /*
     * Mediator is a JobOffer object.
     * Getters and setters used to store a JobOffer when navigating from JobViewer to EmployerProfile
     * (by clicking on a company from a job offer).
     */
    public static JobOffer getJobMediator() {
        return jobMediator;
    }

    public static void setJobMediator(JobOffer jobMediator) {
        DAO.jobMediator = jobMediator;
    }

    public static Employer getEmployerByLogo(String logo) {
        Map<String, Employer> logoEmployerMap = new HashMap<>();

        logoEmployerMap.put("Cobham.jpg",       new Employer(getCompanyTypes().get(4), "2314253644", "CobHam",               "Klamperborgvej",   2080, "Lyngby",     "91830034", "cobham@email.com", "Cobham.jpg"));
        logoEmployerMap.put("sonion.jpg",       new Employer(getCompanyTypes().get(5), "2310654200", "Sonion",               "Roskilde",         9080, "Roskilde",   "91830034", "sonion@email.com", "sonion.jpg"));
        logoEmployerMap.put("coca-cola.png",    new Employer(getCompanyTypes().get(4), "2314253644", "Coca-Cola",            "Copenhagen",       1260, "Copenhagen", "91830034", "coca-cola@email.com", "coca-cola.png"));
        logoEmployerMap.put("McDonalds.png",    new Employer(getCompanyTypes().get(6), "9087430004", "McDonalds",            "Copenhagen",       1570, "Copenhagen", "91830034", "mcd@email.com", "McDonalds.png"));
        logoEmployerMap.put("imerco.jpg",       new Employer(getCompanyTypes().get(4), "2314253644", "Imerco",               "Odense",           2080, "Odense",     "91830034", "imerco@email.com", "imerco.jpg"));
        logoEmployerMap.put("Aarhus_University.png", new Employer(getCompanyTypes().get(2), "7890933200", "Aarhus University",    "Aarhus",           2080, "Aarhus",     "91830034", "aarhus@email.com", "Aarhus_University.png"));
        logoEmployerMap.put("FinPro.jpg",       new Employer(getCompanyTypes().get(1), "2314253644", "FinPro",               "Helsingor",        2080, "Helsingor",  "91830034", "FinPro@email.com", "FinPro.jpg"));
        logoEmployerMap.put("massage.png",     new Employer(getCompanyTypes().get(1), "2314253644", "MassPro",              "Roskilde",         2080, "Roskilde",   "91830034", "MassPro@email.com", "massage.png"));
        logoEmployerMap.put("Rigshospital.png", new Employer(getCompanyTypes().get(1), "2314253644", "Rigshospitalet",       "Copenhagen",       1020, "Copenhagen", "91830034", "rigshospitalet@email.com", "Rigshospital.png"));

        if(logoEmployerMap.containsKey(logo))
            return logoEmployerMap.get(logo);
        else return new Employer(getCompanyTypes().get(0), "0000000000", "default",               "Default address",   2080, "Lyngby",     "91830034", "default@email.com", "logo.jpg");
    }

}
