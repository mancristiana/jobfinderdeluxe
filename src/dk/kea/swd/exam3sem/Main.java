package dk.kea.swd.exam3sem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Navigator.setNavigator(primaryStage);
        Navigator.show(Navigator.Page.Menu);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
