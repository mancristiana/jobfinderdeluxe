package dk.kea.swd.exam3sem;

/**
 * Created by Cris on 30-Nov-15.
 */
public class JobSeeker {
    private int jobSeekerID;
    private String name;
    private Education education;
    private WorkExperience experience;
    private int expYears;

    public  JobSeeker() {
    }

    public JobSeeker(int jobSeekerID, String name, Education education, WorkExperience experience, int expYears) {
        this.jobSeekerID = jobSeekerID;
        this.name = name;
        this.education = education;
        this.experience = experience;
        this.expYears = expYears;
    }

    public int getJobSeekerID() {
        return jobSeekerID;
    }

    public void setJobSeekerID(int jobSeekerID) {
        this.jobSeekerID = jobSeekerID;
    }

    public WorkExperience getExperience() {
        return experience;
    }

    public void setExperience(WorkExperience experience) {
        this.experience = experience;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExpYears() {
        return expYears;
    }

    public void setExpYears(int expYears) {
        this.expYears = expYears;
    }
}
